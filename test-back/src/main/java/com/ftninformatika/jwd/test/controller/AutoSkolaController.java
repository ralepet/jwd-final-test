package com.ftninformatika.jwd.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.AutoSkolaDto;
import com.ftninformatika.jwd.test.dto.PolaganjeDto;
import com.ftninformatika.jwd.test.model.AutoSkola;
import com.ftninformatika.jwd.test.model.Polaganje;
import com.ftninformatika.jwd.test.service.AutoSkolaService;
import com.ftninformatika.jwd.test.support.AutoSkolaDtoToAutoSkola;
import com.ftninformatika.jwd.test.support.AutoSkolaToAutoSkolaDto;
import com.ftninformatika.jwd.test.support.PolaganjeToPolaganjeDto;


@RestController
@RequestMapping(value = "/api/auto-skole", produces = MediaType.APPLICATION_JSON_VALUE)
public class AutoSkolaController {
	
	@Autowired
	private AutoSkolaService autoSkolaService;
	
	@Autowired
	private AutoSkolaToAutoSkolaDto toAutoSkolaDto; 
	
	@Autowired
	private AutoSkolaDtoToAutoSkola toAutoSkola;
	
	@Autowired
	private PolaganjeToPolaganjeDto toPolaganjeDto; 
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@GetMapping
	public ResponseEntity<List<AutoSkolaDto>> getAll() {

		List<AutoSkola> list = autoSkolaService.findAll();

		return new ResponseEntity<>(toAutoSkolaDto.convert(list), HttpStatus.OK);
	}
	
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@GetMapping("/{id}")
	public ResponseEntity<AutoSkolaDto> getOne(@PathVariable Long id) {
		AutoSkola autoSkola = autoSkolaService.findOneById(id); 

		if (autoSkola != null) {
			return new ResponseEntity<>(toAutoSkolaDto.convert(autoSkola), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@GetMapping("/{id}/polaganja")
	public ResponseEntity<List<PolaganjeDto>> getPolaganja(@PathVariable Long id) {
		AutoSkola autoSkola = autoSkolaService.findOneById(id); 

		if (autoSkola != null) {
			
			List<Polaganje> listaPolaganja = autoSkolaService.findAllPolaganja(id);
			return new ResponseEntity<>(toPolaganjeDto.convert(listaPolaganja), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	
	

}
