package com.ftninformatika.jwd.test.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.PolaznikDto;
import com.ftninformatika.jwd.test.model.Polaznik;
import com.ftninformatika.jwd.test.service.PolaznikService;
import com.ftninformatika.jwd.test.support.PolaznikDtoToPolaznik;
import com.ftninformatika.jwd.test.support.PolaznikToPolaznikDto;

@RestController
@RequestMapping(value = "/api/polaznici", produces = MediaType.APPLICATION_JSON_VALUE)
public class PolaznikController {
	
	@Autowired
	private PolaznikService polaznikService;
	
	@Autowired
	private PolaznikDtoToPolaznik toPolaznik;
	
	@Autowired
	private PolaznikToPolaznikDto toDto;
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@GetMapping
	public ResponseEntity<List<PolaznikDto>> getAll(
			@RequestParam(required=false) String ime,  
			@RequestParam(required=false) Long autoSkolaId,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {


		Page<Polaznik> page = polaznikService.find(ime, autoSkolaId, pageNo);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(page.getContent()), headers, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@GetMapping("/{id}")
	public ResponseEntity<PolaznikDto> getOne(@PathVariable Long id) {
		Polaznik polaznik = polaznikService.findOneById(id);

		if (polaznik != null) {
			return new ResponseEntity<>(toDto.convert(polaznik), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PolaznikDto> create(@Valid @RequestBody PolaznikDto polaznikDto) {
		Polaznik polaznik = toPolaznik.convert(polaznikDto); 

		Polaznik sacuvanPolaznik = polaznikService.save(polaznik);

		return new ResponseEntity<>(toDto.convert(sacuvanPolaznik), HttpStatus.CREATED);
	}

	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PolaznikDto> update(@PathVariable Long id, @Valid @RequestBody PolaznikDto poolaznikDto) {

		if (!id.equals(poolaznikDto.getId())) { 
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
 
		Polaznik polaznik = toPolaznik.convert(poolaznikDto); 
		Polaznik sacuvanPolaznik = polaznikService.update(polaznik);

		return new ResponseEntity<>(toDto.convert(sacuvanPolaznik), HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@PutMapping(value = "/{id}/promeni_stanje")
	public ResponseEntity<PolaznikDto> changeState(@PathVariable Long id) {

		Polaznik polaznik = polaznikService.findOneById(id);

		if (polaznik == null || polaznik.isPolozio()== true) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Polaznik polaznikSaImenama = polaznikService.changeState(polaznik); // umesto update treba drugu metodu

		return new ResponseEntity<>(toDto.convert(polaznikSaImenama), HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Polaznik obrisanPolaznik = polaznikService.delete(id); 

		if (obrisanPolaznik != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	
}
