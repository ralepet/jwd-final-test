package com.ftninformatika.jwd.test.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.PrijavaDto;
import com.ftninformatika.jwd.test.model.Polaganje;
import com.ftninformatika.jwd.test.model.Polaznik;
import com.ftninformatika.jwd.test.model.Prijava;
import com.ftninformatika.jwd.test.service.PolaganjeService;
import com.ftninformatika.jwd.test.service.PolaznikService;
import com.ftninformatika.jwd.test.service.PrijavaService;
import com.ftninformatika.jwd.test.support.PrijavaDtoToPrijava;
import com.ftninformatika.jwd.test.support.PrijavaToPrijavaDto;

@RestController
@RequestMapping(value = "/api/prijave", produces = MediaType.APPLICATION_JSON_VALUE)
public class PrijavaController {
	
	@Autowired
	private PrijavaService prijavaService;
	
	@Autowired
	private PolaznikService polaznikService;
	
	@Autowired
	private PolaganjeService polaganjeService;
	
	@Autowired
	private PrijavaDtoToPrijava toPrijava;
	
	@Autowired
	private PrijavaToPrijavaDto toDto;
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@GetMapping
	public ResponseEntity<List<PrijavaDto>> getAll() {

		List<Prijava> list = prijavaService.findAll();

		return new ResponseEntity<>(toDto.convert(list), HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@GetMapping("/{id}")
	public ResponseEntity<PrijavaDto> getOne(@PathVariable Long id) {
		Prijava prijava = prijavaService.findOneById(id); 

		if (prijava != null) {
			return new ResponseEntity<>(toDto.convert(prijava), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrijavaDto> create(@Valid @RequestBody PrijavaDto prijavaDto) { 
		
		Polaganje polaganje = polaganjeService.findOneById(prijavaDto.getPolaganjeId());
		Polaznik polaznik = polaznikService.findOneById(prijavaDto.getPolaznikId());
		// PRVO TREBA PROVERITI DA LI POSTOJE POLAGANJE I POLAZNIK SA ZADATIM ID
		// PROVVERA DA LI POLAGANJE PRIPADA AUTO SKOLI U KOJOJ POLAZNIK POLAZE
		if( polaganje == null || polaznik == null || polaganje.getAutoSkola().getId() != polaznik.getAutoSkola().getId()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Prijava prijava = toPrijava.convert(prijavaDto);

		Prijava sacuvanaPrijava = prijavaService.save(prijava);
		
		if(sacuvanaPrijava == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(toDto.convert(sacuvanaPrijava), HttpStatus.CREATED);
	}
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

}
