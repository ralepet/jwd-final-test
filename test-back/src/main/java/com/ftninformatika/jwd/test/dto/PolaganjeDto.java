package com.ftninformatika.jwd.test.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class PolaganjeDto {
	
	private Long id;

	@NotNull
	@Positive
	private Integer brojMesta;
	
	private String datum;
	
	private Long autoSkolaId;
	
	private String autoskolaNaziv;

	public PolaganjeDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBrojMesta() {
		return brojMesta;
	}

	public void setBrojMesta(Integer brojMesta) {
		this.brojMesta = brojMesta;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public Long getAutoSkolaId() {
		return autoSkolaId;
	}

	public void setAutoSkolaId(Long autoSkolaId) {
		this.autoSkolaId = autoSkolaId;
	}

	public String getAutoskolaNaziv() {
		return autoskolaNaziv;
	}

	public void setAutoskolaNaziv(String autoskolaNaziv) {
		this.autoskolaNaziv = autoskolaNaziv;
	}
	
	
	
	

}
