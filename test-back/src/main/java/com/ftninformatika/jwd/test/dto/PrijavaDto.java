package com.ftninformatika.jwd.test.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class PrijavaDto {
	
	private Long id;
	
	@Positive
	@NotNull
	private Long polaznikId;
	
	private String polaznikIme;
	
	private String polaznikPrezime;
	
	@Positive
	@NotNull
	private Long polaganjeId;
	
	private String polaganjeDatum;

	public PrijavaDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPolaznikId() {
		return polaznikId;
	}

	public void setPolaznikId(Long polaznikId) {
		this.polaznikId = polaznikId;
	}

	public String getPolaznikIme() {
		return polaznikIme;
	}

	public void setPolaznikIme(String polaznikIme) {
		this.polaznikIme = polaznikIme;
	}

	public String getPolaznikPrezime() {
		return polaznikPrezime;
	}

	public void setPolaznikPrezime(String polaznikPrezime) {
		this.polaznikPrezime = polaznikPrezime;
	}

	public Long getPolaganjeId() {
		return polaganjeId;
	}

	public void setPolaganjeId(Long polaganjeId) {
		this.polaganjeId = polaganjeId;
	}

	public String getPolaganjeDatum() {
		return polaganjeDatum;
	}

	public void setPolaganjeDatum(String polaganjeDatum) {
		this.polaganjeDatum = polaganjeDatum;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrijavaDto other = (PrijavaDto) obj;
		return Objects.equals(id, other.id);
	}
	

}
