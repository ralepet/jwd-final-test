package com.ftninformatika.jwd.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.test.model.AutoSkola;

@Repository
public interface AutoSkolaRepository extends JpaRepository<AutoSkola, Long>{
	
	AutoSkola findOneById(Long id);

}
