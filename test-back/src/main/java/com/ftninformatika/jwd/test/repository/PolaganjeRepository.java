package com.ftninformatika.jwd.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.test.model.Polaganje;

@Repository
public interface PolaganjeRepository extends JpaRepository<Polaganje, Long>{
	
	Polaganje findOneById(Long id);

}
