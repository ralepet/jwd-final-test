package com.ftninformatika.jwd.test.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.test.model.Polaznik;

@Repository
public interface PolaznikRepository extends JpaRepository<Polaznik, Long>{
	
	Polaznik findOneById(Long id);

	Page<Polaznik> findByImeIgnoreCaseContains( String Ime, Pageable pageable);

	Page<Polaznik> findByImeIgnoreCaseContainsAndAutoSkolaId(String ime, Long autoSkolaId, Pageable pageable);

}
