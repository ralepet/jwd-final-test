package com.ftninformatika.jwd.test.service;

import java.util.List;

import com.ftninformatika.jwd.test.model.AutoSkola;
import com.ftninformatika.jwd.test.model.Polaganje;

public interface AutoSkolaService {
	
	AutoSkola findOneById(Long id);

	List<AutoSkola> findAll();
	
	List<Polaganje> findAllPolaganja(Long id);
	
	AutoSkola izmeniPolaganja(AutoSkola autoSkola);
	
	

}
