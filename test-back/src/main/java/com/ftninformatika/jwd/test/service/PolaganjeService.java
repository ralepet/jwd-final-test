package com.ftninformatika.jwd.test.service;

import java.util.List;

import com.ftninformatika.jwd.test.model.Polaganje;

public interface PolaganjeService {
	
	Polaganje findOneById(Long id);

	List<Polaganje> findAll();
	
	List<Polaganje> findAllForAutoSkola(Long idAutoskole);

}
