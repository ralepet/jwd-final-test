package com.ftninformatika.jwd.test.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.test.model.Polaznik;


public interface PolaznikService {
	
	Polaznik findOneById(Long id);

	Polaznik save(Polaznik polaznik); 

	Polaznik update(Polaznik polaznik); 

	Polaznik delete(Long id);
	
	Page<Polaznik> findAll(Integer pageNo);

	List<Polaznik> findAll();
	
	Page<Polaznik> find(String ime, Long autoSkolaId, Integer pageNo);
	
	Polaznik changeState(Polaznik polaznik); 
	
	

}
