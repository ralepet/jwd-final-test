package com.ftninformatika.jwd.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.AutoSkola;
import com.ftninformatika.jwd.test.model.Polaganje;
import com.ftninformatika.jwd.test.repository.AutoSkolaRepository;
import com.ftninformatika.jwd.test.service.AutoSkolaService;
import com.ftninformatika.jwd.test.service.PolaganjeService;

@Service
public class JpaAutoSkolaService implements AutoSkolaService{
	
	@Autowired
	private AutoSkolaRepository  autoSkolaRepository;
	
	@Autowired
	private PolaganjeService  polaganjeService;
	

	@Override
	public AutoSkola findOneById(Long id) {
		return autoSkolaRepository.findOneById(id);
	}

	@Override
	public List<AutoSkola> findAll() {
		return autoSkolaRepository.findAll();
	}
	
	@Override
	public List<Polaganje> findAllPolaganja(Long id) {
		return polaganjeService.findAllForAutoSkola(id);
	}

	@Override
	public AutoSkola izmeniPolaganja(AutoSkola autoSkola) {
		autoSkola.getPolaganja();
		return null;
	}

}
