package com.ftninformatika.jwd.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.AutoSkola;
import com.ftninformatika.jwd.test.model.Polaganje;
import com.ftninformatika.jwd.test.repository.AutoSkolaRepository;
import com.ftninformatika.jwd.test.repository.PolaganjeRepository;
import com.ftninformatika.jwd.test.service.PolaganjeService;

@Service
public class JpaPolaganjeService implements PolaganjeService{
	
	@Autowired
	private PolaganjeRepository polaganjeRepository;
	
	@Autowired
	private AutoSkolaRepository autoSkolaRepository;
	

	@Override
	public Polaganje findOneById(Long id) {
		return polaganjeRepository.findOneById(id);
	}

	@Override
	public List<Polaganje> findAll() {
		return polaganjeRepository.findAll();
	}

	@Override
	public List<Polaganje> findAllForAutoSkola(Long idAutoskole) {
		AutoSkola autoskola = autoSkolaRepository.findOneById(idAutoskole);
		return autoskola.getPolaganja();
	}
	
	

}
