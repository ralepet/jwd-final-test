package com.ftninformatika.jwd.test.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Polaznik;
import com.ftninformatika.jwd.test.repository.PolaznikRepository;
import com.ftninformatika.jwd.test.service.PolaznikService;

@Service
public class JpaPolaznikService implements PolaznikService{
	
	@Autowired
	private PolaznikRepository polaznikRepository;

	@Override
	public Polaznik findOneById(Long id) {
		return polaznikRepository.findOneById(id);
	}

	@Override
	public Polaznik save(Polaznik polaznik) {
		return polaznikRepository.save(polaznik);
	}

	@Override
	public Polaznik update(Polaznik polaznik) {
		return polaznikRepository.save(polaznik);
	}

	@Override
	public Polaznik delete(Long id) {
		Optional<Polaznik> polaznik = polaznikRepository.findById(id);
		if (polaznik.isPresent()) {
			polaznikRepository.deleteById(id);
			return polaznik.get();
		}
		return null;
	}
	
	@Override
	public Page<Polaznik> findAll(Integer pageNo) {
		// TODO Auto-generated method stub
		return polaznikRepository.findAll(PageRequest.of(pageNo, 3));
	}

	@Override
	public List<Polaznik> findAll() {
		return polaznikRepository.findAll();
	}

	@Override
	public Page<Polaznik> find(String ime, Long autoSkolaId, Integer pageNo) {
		
		if(ime == null) {
			ime = "";
		}
		
		if(autoSkolaId == null) {
			return polaznikRepository.findByImeIgnoreCaseContains(ime, PageRequest.of(pageNo, 3));
		}
		return polaznikRepository.findByImeIgnoreCaseContainsAndAutoSkolaId(ime, autoSkolaId, PageRequest.of(pageNo, 3));
	}

	@Override
	public Polaznik changeState(Polaznik polaznik) {
		
		if(!polaznik.isOdslusaoTeoriju()) {
			polaznik.setOdslusaoTeoriju(true);
		} else if(!polaznik.isOdradioVoznju()) {
			polaznik.setOdradioVoznju(true); 
		} else if(!polaznik.isPolozio()) {
			polaznik.setPolozio(true); 
		}
		
		return polaznikRepository.save(polaznik);
	}

}
