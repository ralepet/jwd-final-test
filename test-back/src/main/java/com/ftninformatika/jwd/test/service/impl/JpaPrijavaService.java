package com.ftninformatika.jwd.test.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Polaganje;
import com.ftninformatika.jwd.test.model.Polaznik;
import com.ftninformatika.jwd.test.model.Prijava;
import com.ftninformatika.jwd.test.repository.PolaganjeRepository;
import com.ftninformatika.jwd.test.repository.PolaznikRepository;
import com.ftninformatika.jwd.test.repository.PrijavaRepository;
import com.ftninformatika.jwd.test.service.PrijavaService;

@Service
public class JpaPrijavaService implements PrijavaService{
	
	@Autowired
	private PrijavaRepository prijavaRepository;
	
	@Autowired
	private PolaganjeRepository polaganjeRepository;
	
	@Autowired
	private PolaznikRepository polaznikRepository;

	@Override
	public Prijava findOneById(Long id) {
		return prijavaRepository.findOneById(id);
	}

	@Override
	public Prijava save(Prijava prijava) {
		Polaganje polaganje = prijava.getPolaganje();
		Polaznik polaznik = prijava.getPolaznik();
		// ukoliko ima slobodnih mesta za to polaganje i ukoliko korisnik nije prijavljen
		if(polaganje.getBrojMesta() > 0 && polaznik.isPrijavljen() == false) { 
			polaganje.setBrojMesta(polaganje.getBrojMesta() - 1);
			polaganjeRepository.save(polaganje);
			polaznik.setPrijavljen(true);
			polaznikRepository.save(polaznik);
			return prijavaRepository.save(prijava);
		}
		return null;
	}

	@Override
	public Prijava update(Prijava prijava) {
		return prijavaRepository.save(prijava);
	}

	@Override
	public Prijava delete(Long id) {
		Optional<Prijava> prijava = prijavaRepository.findById(id); 
		if (prijava.isPresent()) {
			prijavaRepository.deleteById(id);
			return prijava.get();
		}
		return null;
	}

	@Override
	public List<Prijava> findAll() {
		return prijavaRepository.findAll();
	}

}
