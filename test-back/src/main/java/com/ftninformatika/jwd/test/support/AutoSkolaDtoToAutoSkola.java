package com.ftninformatika.jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.AutoSkolaDto;
import com.ftninformatika.jwd.test.model.AutoSkola;
import com.ftninformatika.jwd.test.service.AutoSkolaService;

@Component
public class AutoSkolaDtoToAutoSkola implements Converter<AutoSkolaDto, AutoSkola> {
	
	@Autowired
	private AutoSkolaService autoSkolaService;

	@Override
	public AutoSkola convert(AutoSkolaDto dto) {
		AutoSkola entity; 
		if (dto.getId() == null) {
			entity = new AutoSkola();
		} else {
			entity = autoSkolaService.findOneById(dto.getId());
		}
		
		entity.setNaziv(dto.getNaziv());
		entity.setBrojVozila(dto.getBrojVozila());
		entity.setGodinaOsnivanja(dto.getGodinaOsnivanja());
		return entity;
	}

}
