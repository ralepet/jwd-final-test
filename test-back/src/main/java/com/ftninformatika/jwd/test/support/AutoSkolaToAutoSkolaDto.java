package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.AutoSkolaDto;
import com.ftninformatika.jwd.test.model.AutoSkola;

@Component
public class AutoSkolaToAutoSkolaDto implements Converter<AutoSkola, AutoSkolaDto> {

	@Override
	public AutoSkolaDto convert(AutoSkola source) {
		AutoSkolaDto dto = new AutoSkolaDto();
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		dto.setBrojVozila(source.getBrojVozila());
		dto.setGodinaOsnivanja(source.getGodinaOsnivanja());
		return dto;
	}
	
	public List<AutoSkolaDto> convert(List<AutoSkola>list){
		List<AutoSkolaDto>dto = new ArrayList<AutoSkolaDto>();
		for(AutoSkola atukoSkola : list) {  
			dto.add(convert(atukoSkola));
		}
		return dto;
	}
	
	

}
