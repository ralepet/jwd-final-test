package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.PolaganjeDto;
import com.ftninformatika.jwd.test.model.Polaganje;

@Component
public class PolaganjeToPolaganjeDto implements Converter<Polaganje, PolaganjeDto>{

	@Override
	public PolaganjeDto convert(Polaganje source) {
		PolaganjeDto dto = new PolaganjeDto();
		dto.setId(source.getId());
		dto.setAutoSkolaId(source.getAutoSkola().getId());
		dto.setAutoskolaNaziv(source.getAutoSkola().getNaziv());
		dto.setBrojMesta(source.getBrojMesta());
		dto.setDatum(source.getDatumPrijave().toString());
		return dto;
	}
	
	public List<PolaganjeDto> convert(List<Polaganje>list){
		List<PolaganjeDto>dto = new ArrayList<PolaganjeDto>();
		for(Polaganje polaganje : list) {   
			dto.add(convert(polaganje));
		}
		return dto;
	}

}
