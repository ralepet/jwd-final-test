package com.ftninformatika.jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.PolaznikDto;
import com.ftninformatika.jwd.test.model.AutoSkola;
import com.ftninformatika.jwd.test.model.Polaznik;
import com.ftninformatika.jwd.test.service.AutoSkolaService;
import com.ftninformatika.jwd.test.service.PolaznikService;

@Component
public class PolaznikDtoToPolaznik implements Converter<PolaznikDto, Polaznik>{
	
	@Autowired
	private PolaznikService polaznikService;
	
	@Autowired
	private AutoSkolaService autoSkolaService;

	@Override
	public Polaznik convert(PolaznikDto dto) {
		
		Polaznik polaznik = null;

		if (dto.getId() != null) {
			polaznik = polaznikService.findOneById(dto.getId());
		}
		
		if(polaznik == null) {
			polaznik = new Polaznik();
        }
		
		polaznik.setGodinaRodjenja(dto.getGodinaRodjenja());
		polaznik.setIme(dto.getIme());
		polaznik.setPrezime(dto.getPrezime());
		polaznik.setMesto(dto.getMesto());
		
		polaznik.setOdslusaoTeoriju(dto.isOdslusaoTeoriju());
		polaznik.setOdradioVoznju(dto.isOdradioVoznju());
		polaznik.setPrijavljen(dto.isPrijavljen());
		polaznik.setPolozio(dto.isPolozio());

		
		AutoSkola autoSkola = autoSkolaService.findOneById(dto.getAutoSkolaId());
		if(autoSkola != null) { 
			polaznik.setAutoSkola(autoSkola);
		}	
			
		return polaznik;
	}

}
