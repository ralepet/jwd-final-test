package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.PolaznikDto;
import com.ftninformatika.jwd.test.model.Polaznik;

@Component
public class PolaznikToPolaznikDto implements Converter<Polaznik, PolaznikDto>{

	@Override
	public PolaznikDto convert(Polaznik source) {
		PolaznikDto dto = new PolaznikDto();
		dto.setId(source.getId());
		dto.setAutoSkolaId(source.getAutoSkola().getId());
		dto.setAutoSkolaNaziv(source.getAutoSkola().getNaziv());
		dto.setGodinaRodjenja(source.getGodinaRodjenja());
		dto.setIme(source.getIme());
		dto.setPrezime(source.getPrezime());
		dto.setMesto(source.getMesto());
		dto.setOdslusaoTeoriju(source.isOdslusaoTeoriju());
		dto.setOdradioVoznju(source.isOdradioVoznju());
		dto.setPrijavljen(source.isPrijavljen());
		dto.setPolozio(source.isPolozio());

		return dto;
	}
	
	public List<PolaznikDto> convert(List<Polaznik>list){
		List<PolaznikDto>dto = new ArrayList<PolaznikDto>();
		for(Polaznik polaznik : list) {    
			dto.add(convert(polaznik));
		}
		return dto;
	}

}
