package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.PrijavaDto;
import com.ftninformatika.jwd.test.model.Prijava;

@Component
public class PrijavaToPrijavaDto implements Converter<Prijava, PrijavaDto>{

	@Override
	public PrijavaDto convert(Prijava source) {
		PrijavaDto dto = new PrijavaDto();
		dto.setId(source.getId());
		dto.setPolaganjeId(source.getPolaganje().getId());
		dto.setPolaganjeDatum(source.getPolaganje().getDatumPrijave().toString());
		dto.setPolaznikId(source.getPolaznik().getId());
		dto.setPolaznikIme(source.getPolaznik().getIme());
		dto.setPolaznikPrezime(source.getPolaznik().getPrezime());
		return dto;
	}
	
	public List<PrijavaDto> convert(List<Prijava>list){
		List<PrijavaDto>dto = new ArrayList<PrijavaDto>();
		for(Prijava prijava : list) {   
			dto.add(convert(prijava));
		}
		return dto;
	}

}
