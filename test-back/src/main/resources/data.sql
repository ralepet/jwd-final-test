INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');

INSERT INTO auto_skola (id, broj_vozila, godina_osnivanja, naziv) VALUES (1, 20, 2015, 'Gazela');
INSERT INTO auto_skola (id, broj_vozila, godina_osnivanja, naziv) VALUES (2, 21, 2016, 'Zebra');
INSERT INTO auto_skola (id, broj_vozila, godina_osnivanja, naziv) VALUES (3, 22, 2013, 'Auto Rad');
INSERT INTO auto_skola (id, broj_vozila, godina_osnivanja, naziv) VALUES (4, 23, 2012, 'Puma');

INSERT INTO polaganje (id, broj_mesta, datum_prijave, auto_skola_id) VALUES (1, 2, '2022-09-17', 1);
INSERT INTO polaganje (id, broj_mesta, datum_prijave, auto_skola_id) VALUES (2, 15, '2022-09-18', 1);
INSERT INTO polaganje (id, broj_mesta, datum_prijave, auto_skola_id) VALUES (3, 16, '2022-09-19', 1);
INSERT INTO polaganje (id, broj_mesta, datum_prijave, auto_skola_id) VALUES (4, 20, '2022-09-07', 2);
INSERT INTO polaganje (id, broj_mesta, datum_prijave, auto_skola_id) VALUES (5, 15, '2022-09-08', 2);
INSERT INTO polaganje (id, broj_mesta, datum_prijave, auto_skola_id) VALUES (6, 16, '2022-09-09', 2);
INSERT INTO polaganje (id, broj_mesta, datum_prijave, auto_skola_id) VALUES (7, 20, '2022-09-07', 3);
INSERT INTO polaganje (id, broj_mesta, datum_prijave, auto_skola_id) VALUES (8, 15, '2022-09-08', 3);
INSERT INTO polaganje (id, broj_mesta, datum_prijave, auto_skola_id) VALUES (9, 16, '2022-10-09', 3);
INSERT INTO polaganje (id, broj_mesta, datum_prijave, auto_skola_id) VALUES (10, 16, '2022-10-11', 3);


INSERT INTO polaznik (id, godina_rodjenja, ime, prezime, mesto, odslusao_teoriju, odradio_voznju, prijavljen, polozio, auto_skola_id) 
VALUES (1, 1990, 'Petar','Petrovic', 'Novi Sad', FALSE, FALSE, FALSE, FALSE, 1);
INSERT INTO polaznik (id, godina_rodjenja, ime, prezime, mesto, odslusao_teoriju, odradio_voznju, prijavljen, polozio, auto_skola_id) 
VALUES (2, 1991, 'Mara','Maric', 'Novi Sad', FALSE, FALSE, FALSE, FALSE, 1);
INSERT INTO polaznik (id, godina_rodjenja, ime, prezime, mesto, odslusao_teoriju, odradio_voznju, prijavljen, polozio, auto_skola_id) 
VALUES (3, 1991, 'Pera','Peric', 'Novi Sad', FALSE, FALSE, FALSE, FALSE, 2);
INSERT INTO polaznik (id, godina_rodjenja, ime, prezime, mesto, odslusao_teoriju, odradio_voznju, prijavljen, polozio, auto_skola_id) 
VALUES (4, 1991, 'Mika','Mikic', 'Novi Sad', FALSE, FALSE, FALSE, FALSE, 2);
INSERT INTO polaznik (id, godina_rodjenja, ime, prezime, mesto, odslusao_teoriju, odradio_voznju, prijavljen, polozio, auto_skola_id) 
VALUES (5, 2000, 'Zika','Zikic', 'Novi Sad', FALSE, FALSE, FALSE, FALSE, 3);


