import React from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import TestAxios from '../../apis/TestAxios';
import { withNavigation } from "../../routeconf";

class KreirajVino extends React.Component {

    constructor(props){
        super(props);

        let polaznik = {
            ime: "",
            prezime: "",
            godinaRodjenja: "" ,
            mesto: "", 
            autoSkolaId: "",
        }

        this.state = {
           
          polaznik: polaznik,
            autoSkole: []
        }

    }

    componentDidMount(){
        this.getAutoSkole();
    }

    async getAutoSkole(){
        try{
            let result = await TestAxios.get("/auto-skole");
            let autoSkole = result.data;
            this.setState({autoSkole: autoSkole});
            console.log("uspesno su dobavljene auto skole");
        }catch(error){
            console.log(error);
            alert("Auto skole nisu uspesno dobavljene");
        }
    }

    async kreiraj(e){ 
        e.preventDefault();

        try{
            let polaznik = this.state.polaznik;
            console.log(polaznik)

            let polaznikDto = {
               
                ime: polaznik.ime,
                prezime: polaznik.prezime,
                godinaRodjenja: polaznik.godinaRodjenja,
                mesto: polaznik.mesto,
                autoSkolaId: polaznik.autoSkolaId
            }
            await TestAxios.post("/polaznici", polaznikDto);
            this.props.navigate("/polaznici");
        }catch(error){
            alert("Polaznik nije kreiran");
        }
    }

    valueInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let polaznik = this.state.polaznik;
        polaznik[name] = value;
    
        this.setState({ polaznik: polaznik });
      }
    

    render() {
        return (
          <>
            <Row>
              <Col></Col>
              <Col xs="12" sm="10" md="8">
                <h1>Dodaj novog polaznika</h1>
                <Form>
                  <Form.Label>Ime</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Ime"
                    name="ime"
                    onChange={(e) => this.valueInputChanged(e)}
                    />
                  <Form.Label>Prezime</Form.Label>
                  <Form.Control
                    type="text"
                    name="prezime"
                    placeholder="Prezime"
                    onChange={(e) => this.valueInputChanged(e)}
                    />
                    <Form.Label>Godina rodjenja</Form.Label>
                    <Form.Control
                    type="text"
                    name="godinaRodjenja"
                    placeholder="Godina rodjenja"
                    onChange={(e) => this.valueInputChanged(e)}
                     />
                    <Form.Label>Mesto</Form.Label>
                    <Form.Control
                        type="text"
                        name="mesto"
                        placeholder="Mesto"
                        onChange={(e) => this.valueInputChanged(e)}
                     />

                    <Form.Label>Auto skola</Form.Label> 
                    <Form.Control as="select" name="autoSkolaId" onChange={event => this.valueInputChanged(event)}>
                        <option>Izaberi auto skolu</option>
                        {
                            this.state.autoSkole.map((autoSkola) => {
                                return (
                                    <option key={autoSkola.id} value={autoSkola.id}>{autoSkola.naziv}</option>
                                )
                            })
                        }
                    </Form.Control><br/> 
    
                  <Button style={{ marginTop: "25px" }} onClick={(event)=>{this.kreiraj(event);}}>
                    Kreiraj Polaznika
                  </Button>
                </Form>
              </Col>
              <Col></Col>
            </Row>                           
            
          </>
        );
      }
}

export default withNavigation(KreirajVino);