import React from 'react';
import TestAxios from '../../apis/TestAxios';
import { Row, Col, Button, Table, Form } from 'react-bootstrap'
import './../../index.css';
import { withParams, withNavigation } from '../../routeconf'

class Polaznici extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            polaznici: [], 
            pretraga: { 
                ime: "",
                autoSkolaId: ""
            },
            currentPage:0, 
            totalPages:0,
            autoSkole: []
        }
    }

    componentDidMount() {
        this.getPolaznike(0);
        this.getAutoSkole();
    }

    async getAutoSkole(){
        try{
            let result = await TestAxios.get("/auto-skole");
            let autoSkole = result.data; 
            this.setState({autoSkole: autoSkole});
            console.log("Auto Skole su uspesno dobavljene");
        }catch(error){
            console.log(error);
            alert("Auto Skole  nisu dobavljene");
        } 
    }

    async getPolaznike(pageNo) {
        const config = {
            params: {
                pageNo: pageNo,
                ime: this.state.pretraga.ime,
                autoSkolaId: this.state.pretraga.autoSkolaId
            }
        }
        try {
            let result = await TestAxios.get('/polaznici', config);
            let total_pages = result.headers["total-pages"]
            this.setState({
              polaznici: result.data,
              currentPage: pageNo,
              totalPages: total_pages
            });
          } catch (error) {
            console.log(error);
          }
    }

    idiNaKreiranje() {
        this.props.navigate('/polaznici/kreiraj');
    }

    obrisi(polaznikId) {
        TestAxios.delete('/polaznici/' + polaznikId)
            .then(res => {
                // handle success
                console.log(res);
                alert('Polaznik je uspesno obrisan!');
                window.location.reload();
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Error occured please try again!');
            });
    }

    idiNaPrethodnuStranicu(){
        this.getPolaznike(this.state.currentPage-1) 
    }

    idiNaSledecuStranicu(){ //
        this.getPolaznike(this.state.currentPage+1) 
    }

    onInputChange(event) {
        const name = event.target.name;
        const value = event.target.value

        let pretraga = this.state.pretraga;
        pretraga[name] = value;

        this.setState({ pretraga: pretraga })
    }

    async promeniStanje( polaznik){ 

        try{
            console.log(polaznik)

            await TestAxios.put("/polaznici/" + polaznik.id + "/promeni_stanje", polaznik);
            window.location.reload();
        }catch(error){
            alert("Error ");
        }
    }

    idiNaPrijavu(autoSkolaId, polaznikId) {
        this.props.navigate('/auto-skole/' + autoSkolaId +'/prijava/' + polaznikId);
    }

    renderPolaznike() {
        return this.state.polaznici.map((polaznik, index) => {
            return (
                <tr key={polaznik.id}>
                    <td>{polaznik.ime}</td>
                    <td>{polaznik.prezime}</td>
                    <td>{polaznik.godinaRodjenja}</td>
                    <td>{polaznik.mesto}</td>
                    <td>{polaznik.autoSkolaNaziv}</td>
                    <td>{polaznik.polozio === true ? "da" : "ne"}</td>
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <td><Button variant="danger" onClick={() => this.obrisi(polaznik.id)}>Obrisi</Button></td>
                        : <td></td>}

                    {polaznik.odslusaoTeoriju === false?
                        <td><Button variant="danger" onClick={() => this.promeniStanje(polaznik)}>Odslusao teoriju</Button></td>
                        : null}
                    
                        {polaznik.odslusaoTeoriju === true && polaznik.odradioVoznju === false?
                        <td><Button variant="primary" onClick={() => this.promeniStanje(polaznik)}>Odradio voznju</Button></td>
                        : null}

                        {polaznik.odslusaoTeoriju === true && polaznik.odradioVoznju === true && polaznik.prijavljen === false?
                        <td><Button variant="warning" onClick={() => this.idiNaPrijavu(polaznik.autoSkolaId, polaznik.id)}>Prijava za polaganje</Button></td>
                        : null}

                            {polaznik.odslusaoTeoriju === true && polaznik.odradioVoznju === true && polaznik.prijavljen === true && polaznik.polozio === false?
                        <td><Button variant="warning" onClick={() => this.promeniStanje(polaznik)}>Polozio</Button></td>
                        : null}

                </tr>
            )
        })
    }


    render() {
        return (
            <Col>
                <Row><h1>Polaznici</h1></Row>

                <Form style={{ width: "100%" }}>                    
                   
                    <Form.Label>Auto skola</Form.Label> 
                    <Form.Control as="select"  name="autoSkolaId" onChange={e => this.onInputChange(e)}>
                        <option value="">Izaberi auto skolu</option>
                        {
                            this.state.autoSkole.map((autoSkola) => {
                                return (
                                    <option key={autoSkola.id} value={autoSkola.id}>{autoSkola.naziv}</option>
                                )
                            })
                        }
                    </Form.Control><br/> 
                    <Form.Group>
                        <Form.Label>Ime</Form.Label>
                        <Form.Control name="ime" type="text" placeholder="Ime polaznika" onChange={(e) => this.onInputChange(e)}></Form.Control>
                    </Form.Group>
                    <Button onClick={() => this.getPolaznike(0)}>Pretraga</Button>
                </Form>
                <br /><br />
                <Row>
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <Button variant="success" onClick={() => this.idiNaKreiranje()}>Kreiraj polaznika</Button> : null
                    }
                    <br /><br />
                <Button variant="info" disabled={this.state.currentPage===0} onClick={()=>this.idiNaPrethodnuStranicu()}>Prethodna</Button>
                <Button variant="info" disabled={this.state.currentPage===this.state.totalPages-1} onClick={()=>this.idiNaSledecuStranicu()}>Sledeca</Button>
                </Row>
                <Row>
                    <Table className="table table-striped" style={{ marginTop: 5 }} >
                        <thead className="thead-dark">
                            <tr>
                                <th>Ime</th>
                                <th>Prezime</th> 
                                <th>Godina rodjenja</th>
                                <th>Mesto</th> 
                                <th>Naziv auto skole</th>
                                <th>Polozio</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderPolaznike()}
                        </tbody>
                    </Table>
                </Row>
            </Col>
        );
    }
}

export default withNavigation(withParams(Polaznici));