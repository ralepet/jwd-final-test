import React from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import TestAxios from './../../apis/TestAxios';
import {withParams, withNavigation} from '../../routeconf';

class Prijava extends React.Component {

    constructor(props){
        super(props);

        let prijava = {
            polaznikId: this.props.params.idPolaznika,
            polaganjeId: -1
        }

        this.state = {
            prijava: prijava, idAutoSkole: this.props.params.idAutoSkole, nazivAutoSkole: "", polaganja: [], ime:"", prezime: ""
        }

    }

    async getPolaganja(idAutoSkole){
        try{
            let result = await TestAxios.get("/auto-skole/" + idAutoSkole + "/polaganja" );
            let polaganja = result.data;
            this.setState({polaganja: polaganja});
            console.log("uspesno su dobavljena  polaganja za auto skolu");
        }catch(error){
            console.log(error);
            alert("Nisu dobavljena polaganja za auto skolu");
        }
    }

     // U SLUCAJU DA NAM TREBA I POLAZNIK ZA NESTO 
    async getPolaznikById(idPolaznika){
        try{
            let result = await TestAxios.get("/polaznici/" + idPolaznika );
            let polaznik = result.data;
            this.setState({ime: polaznik.ime, prezime: polaznik.prezime}); // PO POTREBI SE MOGU DODATI IME I PREZIME POLAZNIKA U STATE
            console.log("uspesno je dobavljen polaznik");
        }catch(error){
            console.log(error);
            alert("Error");
        }
    }
    

    componentDidMount() {
        this.getAutoSkolaById(this.props.params.idAutoSkole);
        this.getPolaganja(this.props.params.idAutoSkole);
        this.getPolaznikById(this.props.params.idPolaznika);
     }
     
 
     getAutoSkolaById(autoSkolaID) {
        TestAxios.get('/auto-skole/' + autoSkolaID)
         .then(res => {
             // handle success
             console.log(res);
             this.setState({idAutoSkole: res.data.id, nazivAutoSkole: res.data.naziv});
         })
         .catch(error => {
             // handle error
             console.log(error);
             alert('Auto skola nije dobavljena!');
          });
     }
     

    async prijavi(e){ 
        e.preventDefault();

        try{
            let prijava = this.state.prijava;
            console.log(prijava)

            let prijavaDto = {

            polaznikId: prijava.polaznikId,
            polaganjeId:  prijava.polaganjeId
                
            }
            await TestAxios.post("/prijave/", prijavaDto); 
            this.props.navigate("/polaznici");
        }catch(error){
            alert("Prijava nije uspesna");
        }
    }

    onInputChange(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let prijava = this.state.prijava;
        prijava[name] = value;
    
        this.setState({ prijava: prijava });
    }

    render() {
        return (
          <>
            <Row>
              <Col></Col>
              <Col xs="12" sm="10" md="8">
                <h1>Prijavi polaznika {this.state.ime + " " + this.state.prezime}</h1>
                <Form>
                <Form.Label>Polaganje u auto skoli {this.state.nazivAutoSkole}</Form.Label> 
                    <Form.Control as="select"  name="polaganjeId" onChange={e => this.onInputChange(e)}>
                        <option value="">Izaberi polaganje</option>
                        {
                            this.state.polaganja.map((polaganje) => {
                                return (
                                    <option key={polaganje.id} value={polaganje.id}>{polaganje.datum}</option>
                                )
                            })
                        }
                    </Form.Control><br/> 
    
                  <Button style={{ marginTop: "25px" }} onClick={(event)=>{this.prijavi(event);}}>
                    Prijavi polaznika
                  </Button>
                </Form>
              </Col>
              <Col></Col>
            </Row>                           
            
          </>
        );
      }


}

export default withNavigation(withParams(Prijava));