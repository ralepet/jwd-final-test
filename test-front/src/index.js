import React from 'react';
import ReactDOM from 'react-dom';
import { Navigate, Route, Link, HashRouter as Router, Routes } from 'react-router-dom';
import { Navbar, Nav, Container, Button } from 'react-bootstrap';
import Home from './components/Home';
import Login from './components/authorization/Login'

import KreirajPolaznika from './components/polaznici/KreirajPolaznika';
import Polaznici from './components/polaznici/Polaznici';
import Prijava from './components/polaznici/Prijava';

import NotFound from './components/NotFound';
import { logout } from './services/auth';


class App extends React.Component {

    render() {
        const jwt = window.localStorage.getItem("jwt")
        if(jwt){
            return (
                <div>
                    <Router>
                        <Navbar expand bg="dark" variant="dark">
                            <Navbar.Brand as={Link} to="/">
                                JWD
                            </Navbar.Brand>
                            <Nav>
                                <Nav.Link as={Link} to="/polaznici">
                                    Polaznici
                                </Nav.Link>
                            </Nav>
                            <Button onClick={() => logout()}>Logout</Button>
                        </Navbar>
                        <Container style={{ paddingTop: "10px" }}>
                            <Routes>
                                <Route path="/" element={<Home />} />
                                <Route path="/polaznici" element={<Polaznici />} />
                                {window.localStorage.getItem("role") === "ROLE_ADMIN" ?  <Route path="/polaznici/kreiraj" element={<KreirajPolaznika />} /> 
                                : <Route path="/polaznici/kreiraj" element={<Navigate replace to="/polaznici" />} /> }
                                <Route path="/auto-skole/:idAutoSkole/prijava/:idPolaznika" element={<Prijava />} /> 
                                <Route path="/login" element={<Navigate replace to="/polaznici" />} />
                                <Route path="*" element={<NotFound />} />
                            </Routes>
                        </Container>
                    </Router>
                </div>
            );
        }else{
            return (
                <div>
                    <Router>
                        <Navbar expand bg="dark" variant="dark">
                            <Navbar.Brand as={Link} to="/">
                                JWD
                            </Navbar.Brand>
                            <Nav>
                                <Nav.Link as={Link} to="/login">
                                    Login
                                </Nav.Link>
                            </Nav>
                        </Navbar>
                        <Container style={{ paddingTop: "10px" }}>
                            <Routes>
                                <Route path="/" element={<Home />} />
                                <Route path="/login" element={<Login />} />
                                <Route path="*" element={<Navigate replace to="/login" />} />
                            </Routes>
                        </Container>
                    </Router>
                </div>
            );
        }
        
    }
};


ReactDOM.render(
    <App />,
    document.querySelector('#root')
);
