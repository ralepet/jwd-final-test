import TestAxios from "../apis/TestAxios"
import jwt_decode from "jwt-decode";

export const login = async function(username, password){
    const cred = {
        username : username,
        password : password
    }
    try{
        const ret = await TestAxios.post('korisnici/auth', cred);
        console.log(ret);
        const decoded_jwt=jwt_decode(ret.data)
        window.localStorage.setItem('jwt', ret.data);
        window.localStorage.setItem('role', decoded_jwt.role.authority)
        // window.localStorage.setItem('username', username) // ja sam dodao
        window.location.reload()
    }catch(err){
        console.log(err);
    }
}

export const logout = function(){
    window.localStorage.removeItem("jwt");
    window.localStorage.removeItem("role");
    // window.localStorage.removeItem("username") // ja sam dodao
    window.location.reload()
}